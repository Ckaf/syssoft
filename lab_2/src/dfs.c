#include "dfs.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void dfs_bypass(struct ast_node *node, process_cb preproccess_cb,
                process_cb postprocess_cb) {
  if (node == NULL)
    return;

  node->is_visited = true;
  preproccess_cb(node);
  switch (node->type) {
  case T_PROGRAM: {
    dfs_bypass(node->as_program.child, preproccess_cb, postprocess_cb);
    break;
  }
  case T_EXPR_LIST:
  case T_ARGDEF_LIST:
  case T_ARRAY:
  case T_LIT_LIST:
  case T_STMTS_LIST: {
    struct ast_node *iter = node;
    while (iter != NULL) {
      dfs_bypass(iter->as_list.current, preproccess_cb, postprocess_cb);
      struct ast_node *temp = iter;
      iter = iter->as_list.next;
      // postprocess_cb(temp);
    }
    return;
    // break;
  }
  case T_WHILE:
  case T_REPEAT: {
    dfs_bypass(node->as_repeat.test, preproccess_cb, postprocess_cb);
    dfs_bypass(node->as_repeat.body, preproccess_cb, postprocess_cb);
    break;
  }
  case T_BRANCH: {
    dfs_bypass(node->as_branch.test, preproccess_cb,
               postprocess_cb); // todo add node?
    dfs_bypass(node->as_branch.consequent, preproccess_cb, postprocess_cb);
    dfs_bypass(node->as_branch.alternate, preproccess_cb, postprocess_cb);
    break;
  }
  case T_BIN_EXPR: {
    dfs_bypass(node->as_binexpr.arg1, preproccess_cb, postprocess_cb);
    dfs_bypass(node->as_binexpr.arg2, preproccess_cb, postprocess_cb);

    break;
  }
  case T_UN_EXPR: {
    dfs_bypass(node->as_unexpr.argument, preproccess_cb, postprocess_cb);
    break;
  }
  case T_INDEXER: {
    dfs_bypass(node->as_indexer.expr, preproccess_cb, postprocess_cb);
    dfs_bypass(node->as_indexer.expr_list, preproccess_cb, postprocess_cb);
    break;
  }
  case T_CALL_EXPR: {
    dfs_bypass(node->as_call_expr.expr, preproccess_cb, postprocess_cb);
    dfs_bypass(node->as_call_expr.expr_list, preproccess_cb, postprocess_cb);
    break;
  }
  case T_FUNC_SIGN: {
    dfs_bypass(node->as_func_sign.ident, preproccess_cb, postprocess_cb);
    dfs_bypass(node->as_func_sign.arg_list, preproccess_cb, postprocess_cb);
    break;
  }
  case T_FUNC: {
    dfs_bypass(node->as_func.func_sign, preproccess_cb, postprocess_cb);
    dfs_bypass(node->as_func.body, preproccess_cb, postprocess_cb);
    break;
  }
  case T_IDENT: {
    dfs_bypass(node->as_ident.type, preproccess_cb, postprocess_cb);
    break;
  }
  case T_BODY: {
    dfs_bypass(node->as_body.var_list, preproccess_cb,
               postprocess_cb); // todo rename to var_list
    dfs_bypass(node->as_body.statement, preproccess_cb, postprocess_cb);
    break;
  }

  case T_STMT: {
    dfs_bypass(node->as_statement.some_node, preproccess_cb, postprocess_cb);
    break;
  }
  case T_EXPR: {
    dfs_bypass(node->as_expr.some_node, preproccess_cb, postprocess_cb);
    break;
  }
  case T_TYPE_REF: {
    if (node->as_typeref.type == TR_IDENT || node->as_typeref.type == TR_ARR) {
      dfs_bypass(node->as_typeref.sub_field, preproccess_cb, postprocess_cb);
    }
    break;
  }
  default: {
    break;
  }
  }
  postprocess_cb(node);
  return;
}

static void insert_connect(struct cfg_node *node, struct cfg_node **arr) {
  int i = 0;
  for (; arr[i]; i++)
    ;
  arr[i] = node;
}

static struct cfg_node *create_cfg_node(struct ast_node *node) {
  struct cfg_node *cnode = malloc(sizeof(struct cfg_node));
  memset(cnode->connection_array, NULL, 100 * sizeof(void *));

  cnode->is_visited = false;
  cnode->body = NULL;
  cnode->node = node;
  return cnode;
}

struct cfg_node *cfg_create(struct ast_node *node, struct cfg_node *prev_cfg) {
  if (node == NULL)
    return NULL;

  node->is_visited = true;
  struct cfg_node *cnode = NULL;
  struct cfg_node *ret_val = NULL;
  switch (node->type) {
  case T_PROGRAM: {
    ret_val = cfg_create(node->as_program.child, cnode);
    break;
  }
  case T_EXPR_LIST:
  case T_ARGDEF_LIST:
  case T_ARRAY:
  case T_LIT_LIST:
  case T_STMTS_LIST: {

    struct ast_node *iter = node;
    while (iter != NULL) {
      ret_val = cfg_create(iter->as_list.current, prev_cfg);
      iter = iter->as_list.next;
      // postprocess_cb(temp);
    }
    // if (prev_cfg->node->type == T_WHILE || prev_cfg->node->type == T_WHILE) {
    //   insert_connect(prev_cfg, ret_val->connection_array);
    // }
    //  return cnode;
    break;
  }
  case T_WHILE:
  case T_REPEAT: {
    // cfg_create(node->as_repeat.test, cnode);
    cnode = create_cfg_node(node);
    cnode->body = node->as_repeat.test;

    ret_val = cfg_create(node->as_repeat.body, cnode);
    insert_connect(cnode, ret_val->connection_array);

    break;
  }
  case T_BRANCH: {
    // printf("%s\n", ant_names[prev_cfg->node->type]);
    //  cfg_create(node->as_branch.test); // todo add node?
    cnode = create_cfg_node(node);
    cnode->body = node->as_branch.test;

    ret_val = cfg_create(node->as_branch.consequent, cnode);
    ret_val = cfg_create(node->as_branch.alternate, cnode);
    break;
  }
  case T_BIN_EXPR: {
    cnode = create_cfg_node(node);
    cnode->body = node;
    // if (prev_cfg && prev_cfg->node->type == T_BRANCH) {
    //   // cnode = create_cfg_node(node);
    //   //       insert_connect(prev_ast, cnode->connection_array);
    //   insert_connect(cnode, prev_cfg->connection_array);
    // }

    // cfg_create(node->as_binexpr.arg1);
    // cfg_create(node->as_binexpr.arg2);

    break;
  }
  case T_UN_EXPR: {
    cnode = create_cfg_node(node);
    cnode->body = node;
    // if (prev_cfg && prev_cfg->node->type == T_BRANCH) {
    //   // cnode = create_cfg_node(node);
    //   //       insert_connect(prev_ast, cnode->connection_array);
    //   insert_connect(cnode, prev_cfg->connection_array);
    // }
    //  cfg_create(node->as_unexpr.argument, cnode);
    break;
  }
  case T_INDEXER: {
    // ret_val = cfg_create(node->as_indexer.expr, cnode);
    // ret_val = cfg_create(node->as_indexer.expr_list, cnode);
    break;
  }
  case T_CALL_EXPR: {
    // ret_val = cfg_create(node->as_call_expr.expr, cnode);
    // ret_val = cfg_create(node->as_call_expr.expr_list, cnode);
    break;
  }
  case T_FUNC_SIGN: {
    // cfg_create(node->as_func_sign.ident);
    // cfg_create(node->as_func_sign.arg_list);
    break;
  }
  case T_FUNC: {
    cnode = create_cfg_node(node);
    // cfg_create(node->as_func.func_sign);
    ret_val = cfg_create(node->as_func.body, cnode);
    break;
  }
  case T_IDENT: {
    // cfg_create(node->as_ident.type);
    break;
  }
  case T_BODY: {
    // cfg_create(node->as_body.var_list); // todo rename to var_list
    ret_val = cfg_create(node->as_body.statement, prev_cfg);
    break;
  }

  case T_STMT: {
    // if (node->as_statement.some_node->type == T_EXPR) {
    //   cnode = create_cfg_node(node);
    //   struct ast_node *tmp = node->as_statement.some_node;
    //   cnode->body = tmp->as_expr.some_node;
    // }

    ret_val = cfg_create(node->as_statement.some_node, prev_cfg);
    break;
  }
  case T_EXPR: {
    ret_val = cfg_create(node->as_expr.some_node, prev_cfg);
    break;
  }
  case T_TYPE_REF: {
    // if (node->as_typeref.type == TR_IDENT || node->as_typeref.type == TR_ARR)
    // {
    //   cfg_create(node->as_typeref.sub_field, cnode);
    // }
    break;
  }
  default: {
    break;
  }
  }
  if (cnode && prev_cfg) {
    insert_connect(cnode, prev_cfg->connection_array);
  }
  return ((cnode) ? cnode : ret_val);
  // return cnode;
}
