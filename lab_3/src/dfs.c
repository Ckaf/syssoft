#include "dfs.h"

#include "symbol_table.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void dfs_bypass(struct ast_node *node, process_cb preproccess_cb,
                process_cb postprocess_cb) {
  if (node == NULL)
    return;

  node->is_visited = true;
  preproccess_cb(node);
  switch (node->type) {
  case T_PROGRAM: {
    dfs_bypass(node->as_program.next_func, preproccess_cb, postprocess_cb);
    dfs_bypass(node->as_program.child, preproccess_cb, postprocess_cb);
    break;
  }
  case T_EXPR_LIST:
  case T_ARGDEF_LIST:
  case T_ARRAY:
  case T_LIT_LIST:
  case T_STMTS_LIST: {
    struct ast_node *iter = node;
    while (iter != NULL) {
      dfs_bypass(iter->as_list.current, preproccess_cb, postprocess_cb);
      struct ast_node *temp = iter;
      iter = iter->as_list.next;
      // postprocess_cb(temp);
    }
    return;
    // break;
  }
  case T_WHILE:
  case T_REPEAT: {
    dfs_bypass(node->as_repeat.test, preproccess_cb, postprocess_cb);
    dfs_bypass(node->as_repeat.body, preproccess_cb, postprocess_cb);
    break;
  }
  case T_BRANCH: {
    dfs_bypass(node->as_branch.test, preproccess_cb,
               postprocess_cb); // todo add node?
    dfs_bypass(node->as_branch.consequent, preproccess_cb, postprocess_cb);
    dfs_bypass(node->as_branch.alternate, preproccess_cb, postprocess_cb);
    break;
  }
  case T_BIN_EXPR: {
    dfs_bypass(node->as_binexpr.arg1, preproccess_cb, postprocess_cb);
    dfs_bypass(node->as_binexpr.arg2, preproccess_cb, postprocess_cb);

    break;
  }
  case T_UN_EXPR: {
    dfs_bypass(node->as_unexpr.argument, preproccess_cb, postprocess_cb);
    break;
  }
  case T_INDEXER: {
    dfs_bypass(node->as_indexer.expr, preproccess_cb, postprocess_cb);
    dfs_bypass(node->as_indexer.expr_list, preproccess_cb, postprocess_cb);
    break;
  }
  case T_CALL_EXPR: {
    dfs_bypass(node->as_call_expr.expr, preproccess_cb, postprocess_cb);
    dfs_bypass(node->as_call_expr.expr_list, preproccess_cb, postprocess_cb);
    break;
  }
  case T_FUNC_SIGN: {
    dfs_bypass(node->as_func_sign.ident, preproccess_cb, postprocess_cb);
    dfs_bypass(node->as_func_sign.arg_list, preproccess_cb, postprocess_cb);
    break;
  }
  case T_FUNC: {
    dfs_bypass(node->as_func.func_sign, preproccess_cb, postprocess_cb);
    dfs_bypass(node->as_func.body, preproccess_cb, postprocess_cb);
    break;
  }
  case T_IDENT: {
    dfs_bypass(node->as_ident.type, preproccess_cb, postprocess_cb);
    break;
  }
  case T_BODY: {
    dfs_bypass(node->as_body.var_list, preproccess_cb,
               postprocess_cb); // todo rename to var_list
    dfs_bypass(node->as_body.statement, preproccess_cb, postprocess_cb);
    break;
  }

  case T_STMT: {
    dfs_bypass(node->as_statement.some_node, preproccess_cb, postprocess_cb);
    break;
  }
  case T_EXPR: {
    dfs_bypass(node->as_expr.some_node, preproccess_cb, postprocess_cb);
    break;
  }
  case T_TYPE_REF: {
    if (node->as_typeref.type == TR_IDENT || node->as_typeref.type == TR_ARR) {
      dfs_bypass(node->as_typeref.sub_field, preproccess_cb, postprocess_cb);
    }
    break;
  }
  default: {
    break;
  }
  }
  postprocess_cb(node);
  return;
}

static void insert_connect(struct cfg_node *node, struct cfg_node **arr) {
  int i = 0;
  for (; arr[i]; i++)
    ;

  arr[i] = node;
}

static void set_parent(struct cfg_node *node, struct cfg_node *parent) {
  int i = 0;
  for (; node->parents[i] && i < 100; i++)
    ;
  node->parents[i] = parent;
}

static int get_parents_num(struct cfg_node *node) {
  int i = 0;
  for (; node->parents[i] && i < 100; i++)
    ;
  return i;
}

static struct cfg_node *create_cfg_node(struct ast_node *node) {
  struct cfg_node *cnode = malloc(sizeof(struct cfg_node));
  memset(cnode->connection_array, NULL, 100 * sizeof(void *));

  cnode->is_visited = false;
  cnode->is_visited_asm = false;
  cnode->body = NULL;
  cnode->node = node;
  cnode->parents = calloc(100, sizeof(struct cfg_node *));

  return cnode;
}

struct ast_node *reverse_ast_list(struct ast_node *head) {
  if (head == NULL || head->as_list.next == NULL)
    return head;

  struct ast_node *rest = reverse_ast_list(head->as_list.next);
  head->as_list.next->as_list.next = head;
  head->as_list.next = NULL;

  return rest;
}

struct cfg_node *cfg_create(struct ast_node *node, struct cfg_node *prev_cfg,
                            struct cfg_node *next_node) {
  if (node == NULL)
    return NULL;

  // node->is_visited = true;
  struct cfg_node *cnode = NULL;
  struct cfg_node *ret_val = NULL;
  switch (node->type) {
  case T_PROGRAM: {
    ret_val = cfg_create(node->as_program.child, cnode, NULL);
    break;
  }
  case T_ARGDEF_LIST: {
    struct ast_node *iter = reverse_ast_list(node);
    struct cfg_node *last_cfg = NULL;

    while (iter != NULL) {
      ret_val = cfg_create(iter->as_list.current, NULL, ret_val);
      if (!last_cfg)
        last_cfg = ret_val;
      iter = iter->as_list.next;
    }
    return last_cfg;

    break;
  }
  case T_EXPR_LIST:

  case T_ARRAY:
  case T_LIT_LIST:
  case T_STMTS_LIST: {

    struct ast_node *iter;
    struct cfg_node *prev_iter = prev_cfg;

    // iter = (prev_cfg->node->type == T_FUNC || prev_cfg->node->type ==
    // T_WHILE)
    //            ? reverse_ast_list(node)
    //            : node;

    iter = reverse_ast_list(node);

    struct cfg_node *parent_iter = prev_cfg;
    struct cfg_node *last_cfg = NULL;

    while (iter != NULL) {
      // if (prev_cfg->node->type != T_FUNC && prev_cfg->node->type != T_WHILE)
      //   ret_val = NULL;

      ret_val = cfg_create(iter->as_list.current, NULL, ret_val);
      if (!last_cfg)
        last_cfg = ret_val;

      iter = iter->as_list.next;
    }

    // if (prev_cfg->node->type == T_FUNC || prev_cfg->node->type == T_WHILE)
    insert_connect(ret_val, prev_cfg->connection_array);
    set_parent(ret_val, prev_cfg);

    if (next_node) {
      if (last_cfg->node->type == T_BRANCH) {
        // if ()
        insert_connect(next_node,
                       last_cfg->connection_array[0]->connection_array);
        set_parent(next_node, last_cfg->connection_array[0]);

        if (last_cfg->connection_array[1]) {
          insert_connect(next_node,
                         last_cfg->connection_array[1]->connection_array);
          set_parent(next_node, last_cfg->connection_array[1]);

        } else {
          insert_connect(next_node, last_cfg->connection_array);
          set_parent(next_node, last_cfg);
        }
      } else {
        insert_connect(next_node, last_cfg->connection_array);
        set_parent(next_node, last_cfg);
      }

      // insert_connect(next_node, last_cfg->connection_array);
      // set_parent(next_node, last_cfg);
    }
    return last_cfg;
    // ret_val = last_cfg;
    break;
  }
  case T_WHILE:
  case T_REPEAT: {
    // cfg_create(node->as_repeat.test, cnode);
    cnode = create_cfg_node(node);
    cnode->body = node->as_repeat.test;

    ret_val = cfg_create(node->as_repeat.body, cnode, next_node);
    insert_connect(cnode, ret_val->connection_array);
    set_parent(cnode, prev_cfg);

    break;
  }
  case T_BRANCH: {
    // printf("%s\n", ant_names[prev_cfg->node->type]);
    //  cfg_create(node->as_branch.test); // todo add node?
    cnode = create_cfg_node(node);
    cnode->body = node->as_branch.test;

    ret_val = cfg_create(node->as_branch.consequent, cnode, next_node);
    if (node->as_branch.alternate)
      ret_val = cfg_create(node->as_branch.alternate, cnode, next_node);
    else if (next_node) {
      cnode->connection_array[2] = next_node;
      // insert_connect(next_node, cnode->connection_array);
      // set_parent(cnode, prev_cfg);
      set_parent(next_node, cnode);
    }

    break;
  }
  case T_BIN_EXPR: {
    cnode = create_cfg_node(node);
    cnode->body = node;
    ret_val = cfg_create(node->as_binexpr.arg1, cnode, NULL);
    ret_val = cfg_create(node->as_binexpr.arg2, cnode, NULL);
    if (next_node) {
      insert_connect(next_node, cnode->connection_array);
      set_parent(next_node, cnode);
    }
    break;
  }
  case T_UN_EXPR: {
    cnode = create_cfg_node(node);
    cnode->body = node;
    ret_val = cfg_create(node->as_unexpr.argument, cnode, NULL);
    if (next_node) {
      insert_connect(next_node, cnode->connection_array);
      set_parent(next_node, cnode);
    }

    break;
  }
  case T_INDEXER: {
    // ret_val = cfg_create(node->as_indexer.expr, cnode);
    // ret_val = cfg_create(node->as_indexer.expr_list, cnode);
    break;
  }
  case T_CALL_EXPR: {
    cnode = create_cfg_node(node);
    // cfg_create(node->as_call_expr.expr, cnode, next_node);
    char *name = node->as_call_expr.expr->as_ident.name;
    // ret_val = cfg_create(node->as_call_expr.expr_list, cnode, next_node);
    break;
  }
  case T_FUNC_SIGN: {
    // cfg_create(node->as_func_sign.ident);
    ret_val = cfg_create(node->as_func_sign.arg_list, cnode, next_node);
    break;
  }
  case T_FUNC: {
    cnode = create_cfg_node(node);
    cfg_create(node->as_func.func_sign, cnode, next_node);
    ret_val = cfg_create(node->as_func.body, cnode, next_node);
    break;
  }
  case T_IDENT: {
    cnode = create_cfg_node(node);
    cnode->body = node->as_ident.type;
    ret_val = cfg_create(node->as_ident.type, cnode, next_node);
    break;
  }
  case T_LITERAL: {
    cnode = create_cfg_node(node);
    cnode->body = node->as_literal.type;
    break;
  }
  case T_BODY: {
    // cfg_create(node->as_body.var_list); // todo rename to var_list
    ret_val = cfg_create(node->as_body.statement, prev_cfg, next_node);
    break;
  }

  case T_STMT: {
    // if (node->as_statement.some_node->type == T_EXPR) {
    //   cnode = create_cfg_node(node);
    //   struct ast_node *tmp = node->as_statement.some_node;
    //   cnode->body = tmp->as_expr.some_node;
    // }

    ret_val = cfg_create(node->as_statement.some_node, prev_cfg, next_node);
    break;
  }
  case T_EXPR: {
    ret_val = cfg_create(node->as_expr.some_node, prev_cfg, next_node);
    break;
  }
  case T_TYPE_REF: {
    // if (node->as_typeref.type == TR_IDENT || node->as_typeref.type == TR_ARR)
    // {
    //   cfg_create(node->as_typeref.sub_field, cnode);
    // }
    break;
  }
  default: {
    break;
  }
  }
  if (cnode && prev_cfg) { // && prev_cfg->node->type != T_FUNC
    set_parent(cnode, prev_cfg);
    insert_connect(cnode, prev_cfg->connection_array);
  }
  return ((cnode) ? cnode : ret_val);
  // return cnode;
}

#define ADD_TO_INSTR_LIST(list, instr)                                         \
  do {                                                                         \
    if (!list->head)                                                           \
      list->head = instr;                                                      \
    if (list->tail)                                                            \
      list->tail->next = instr;                                                \
    list->tail = instr;                                                        \
  } while (0)

void MERGE_LIST(instr_list_t *list1, instr_list_t *list2) {
  if (!list1 || !list2)
    return;

  instr_node_t *iter = list2->head;
  while (iter) {
    ADD_TO_INSTR_LIST(list1, iter);
    iter = iter->next;
  }
  list1->tail = list2->tail;
};
stable_t *stable_arr = NULL;
stable_t *stable = NULL;
int get_tr_type_size(enum type_ref type) {
  switch (type) {
  case TR_IDENT:
    return TR_IDENT_SIZE;
  case TR_BOOL:
    return TR_BOOL_SIZE;
  case TR_BYTE:
    return TR_BYTE_SIZE;
  case TR_INT:
    return TR_INT_SIZE;
  case TR_UINT:
    return TR_UINT_SIZE;
  case TR_LONG:
    return TR_LONG_SIZE;
  case TR_ULONG:
    return TR_ULONG_SIZE;
  case TR_CHAR:
    return TR_CHAR_SIZE;
  case TR_STR:
    return TR_STR_SIZE;
  case TR_ARR:
    return TR_ARR_SIZE;
  case TR_NONE:
    return TR_NONE_SIZE;
  }
}
instr_node_t *generate_epiloge(instr_list_t *list) {
  printf("generate_epiloge\n");
  instr_node_t *instr1 = create_instr_node(DP);
  instr1->instruction.dpi = (dpi_t){.instr_type = ADD,
                                    .rd = SP,
                                    .op1 = REG,
                                    .op1_reg = FP,
                                    .op2 = IMV,
                                    .op2_imm_val = 0};

  enum register_type *reglist = malloc(sizeof(enum register_type) * 8);
  reglist[0] = R0;
  reglist[1] = R1;
  reglist[2] = R2;
  reglist[3] = R3;
  reglist[4] = R4;
  reglist[5] = SP;
  reglist[6] = FP;
  reglist[7] = LR;

  instr_node_t *instr2 = create_instr_node(SKI);

  instr2->instruction.ski =
      (ski_t){.instr_type = PUSH, .reglist = reglist, .reglist_size = 8};

  ADD_TO_INSTR_LIST(list, instr1);
  ADD_TO_INSTR_LIST(list, instr2);

  return instr1;
}

instr_node_t *generate_prolog(instr_list_t *list, struct ast_list *var_list,
                              struct ast_list *arg_list) {
  struct ast_list *iter;
  instr_node_t *instr1 = create_instr_node(SKI);
  instr1->type = PROLOG;

  // save register state

  // push {r0, r1, r2, r3, r4, sp, fp, lr}
  enum register_type *reglist = malloc(sizeof(enum register_type) * 8);
  reglist[0] = R0;
  reglist[1] = R1;
  reglist[2] = R2;
  reglist[3] = R3;
  reglist[4] = R4;
  reglist[5] = SP;
  reglist[6] = FP;
  reglist[7] = LR;

  instr1->instruction.ski =
      (ski_t){.instr_type = PUSH, .reglist = reglist, .reglist_size = 8};

  instr_node_t *instr2 = create_instr_node(DP);
  instr2->type = PROLOG;
  // add    r11, sp, #0
  instr2->instruction.dpi = (dpi_t){.instr_type = ADD,
                                    .rd = FP,
                                    .op1 = REG,
                                    .op1_reg = SP,
                                    .op2 = IMV,
                                    .op2_imm_val = 0};

  instr_node_t *instr3 = create_instr_node(DP);
  instr3->type = PROLOG;
  // allocate stack frame
  // sub    sp, sp, #36

  // calculate frame size
  int frame_size = 0;
  iter = var_list;

  if (iter->current != NULL)
    while (iter != (void *)0x8) { // 8 is offset
      frame_size +=
          get_tr_type_size(iter->current->as_ident.type->as_typeref.type);
      iter = &iter->next->as_list;
      printf("iter = %p\n", iter);
    };

  iter = arg_list;

  if (iter->current != NULL)
    while (iter != (void *)0x8) { // 8 is offset
      frame_size +=
          get_tr_type_size(iter->current->as_ident.type->as_typeref.type);
      iter = &iter->next->as_list;
      printf("iter = %p\n", iter);
    };

  frame_size += DEFAULT_FRAME_SIZE;

  instr3->instruction.dpi = (dpi_t){.instr_type = SUB,
                                    .cond = AL,
                                    .rd = SP,
                                    .op1 = REG,
                                    .op1_reg = SP,
                                    .op2 = IMV,
                                    .op2_imm_val = frame_size};
  ADD_TO_INSTR_LIST(list, instr1);
  ADD_TO_INSTR_LIST(list, instr2);
  ADD_TO_INSTR_LIST(list, instr3);

  // instr1->next = instr2;
  // instr2->next = instr3;
  // list->head = instr1;
  // printf("list->head = %p \n", list->head);
  // list->tail = instr3;
  // list->current = instr3;
  // printf("instr head %p\n", list->head);
  iter = var_list;
  int offset = DEFAULT_FRAME_SIZE;

  if (iter && iter->current != NULL)
    while (iter != (void *)0x8) { // 8 is offset
      int size =
          get_tr_type_size(iter->current->as_ident.type->as_typeref.type);
      printf("stable ptr = %p\n", stable);

      stable_set(
          stable,
          (symbol_t){.node = iter->current, .offset = offset, .size = size});
      printf("NNN 2\n");
      printf("var %s\n", iter->current->as_ident.name);
      offset += size;

      iter = &iter->next->as_list;
    };

  iter = arg_list;
  if (iter->current != NULL)
    while (iter != (void *)0x8) { // 8 is offset
      int size =
          get_tr_type_size(iter->current->as_ident.type->as_typeref.type);
      stable_set(
          stable,
          (symbol_t){.node = iter->current, .offset = offset, .size = size});
      printf("var %s\n", iter->current->as_ident.name);
      offset += size;

      iter = &iter->next->as_list;
    };

  return instr1;
}

enum ls_op_type get_load_op(struct ast_node *node) {
  enum ls_op_type opt;
  switch (node->as_typeref.type) {
  case TR_INT: {
    opt = WL;
    break;
  }
  case TR_CHAR: {
    opt = BL;
    break;
  }
  case TR_BYTE: {
    opt = SBL;
    break;
  }
  case TR_UINT: {
    opt = WL;
    break;
  }
  case TR_BOOL: {
    opt = BL;
    break;
  }
  default:
    opt = WL;
  }
  return opt;
}

void read_ident(const symbol_t *s, instr_list_t *instr_list,
                enum register_type rt) {
  // const symbol_t *op = stable_get(stable, s->node);

  enum ls_op_type opt = get_load_op(s->node->as_ident.type);

  instr_node_t *instr = create_instr_node(LDI);
  instr->type = BODY;
  instr->instruction.ldi = (ldi_t){
      .instr_type = LDR, .rt = rt, .lot = opt, .offset = s->offset, .rn = SP};
  ADD_TO_INSTR_LIST(instr_list, instr);
}

void push_word(instr_list_t *instr_list, enum register_type rt) {
  instr_node_t *instr = create_instr_node(SKI);
  enum register_type *reg_list = malloc(sizeof(enum register_type));
  *reg_list = rt;

  instr->instruction.ski =
      (ski_t){.instr_type = PUSH, .reglist = reg_list, .reglist_size = 1};
  ADD_TO_INSTR_LIST(instr_list, instr);
}

void pop_word(instr_list_t *instr_list, enum register_type rt) {
  instr_node_t *instr = create_instr_node(SKI);
  enum register_type *reg_list = malloc(sizeof(enum register_type));
  *reg_list = rt;

  instr->instruction.ski =
      (ski_t){.instr_type = POP, .reglist = reg_list, .reglist_size = 1};
  ADD_TO_INSTR_LIST(instr_list, instr);
}

void branch_instr(instr_list_t *instr_list, enum condition_type cond,
                  char *label) {
  instr_node_t *instr = create_instr_node(JMP);
  instr->instruction.jmi =
      (jmi_t){.instr_type = B, .cond = cond, .label = label};
  ADD_TO_INSTR_LIST(instr_list, instr);
}
void cmp(instr_list_t *instr_list, enum register_type op1,
         enum register_type op2) {
  instr_node_t *instr = create_instr_node(DP);
  instr->instruction.dpi = (dpi_t){.instr_type = CMP,
                                   .s = NONE,
                                   .cond = AL,
                                   .rd = REG_NONE,
                                   .op1 = REG,
                                   .op2 = REG,
                                   .op1_reg = op1,
                                   .op2_reg = op2};
  ADD_TO_INSTR_LIST(instr_list, instr);
}

void label_instr(instr_list_t *instr_list, char *label) {
  instr_node_t *instr = create_instr_node(LABEL);
  instr->instruction.label = label;
  ADD_TO_INSTR_LIST(instr_list, instr);
}

void mov_imm_instr(instr_list_t *instr_list, enum register_type rt,
                   uint16_t imm16) {
  instr_node_t *instr = create_instr_node(DP);
  instr->instruction.dpi = (dpi_t){.instr_type = MOV,
                                   .s = NONE,
                                   .cond = AL,
                                   .op1 = REG,
                                   .op1_reg = rt,
                                   .op2 = IMV,
                                   .op2_imm_val = imm16};
  ADD_TO_INSTR_LIST(instr_list, instr);
}

void movt_instr(instr_list_t *instr_list, enum register_type rt,
                uint16_t imm16) {
  instr_node_t *instr = create_instr_node(DP);
  instr->instruction.dpi = (dpi_t){.instr_type = MOVT,
                                   .s = NONE,
                                   .cond = AL,
                                   .op1 = REG,
                                   .op1_reg = rt,
                                   .op2 = IMV,
                                   .op2_imm_val = imm16};
  ADD_TO_INSTR_LIST(instr_list, instr);
}

void set_label(labels_t *labels, instr_list_t *instr_list) {
  if (labels) {
    char *label = generate_label();

    if (!labels->true_branch)
      labels->true_branch = label;
    else if (!labels->true_branch_end)
      labels->true_branch_end = label;
    else if (!labels->false_branch)
      labels->false_branch = label;
    else if (!labels->false_branch_end)
      labels->false_branch_end = label;

    label_instr(instr_list, label);
  }
}

bool is_end_of_loop(struct cfg_node **connection_array) {
  bool is_end = false;
  for (int i = 0; i < 100; i++) {
    if (!connection_array[i])
      break;
    if (connection_array[i]->is_visited_asm) {
      is_end = true;
      break;
    }
  }
  return is_end;
}
int connection_array_size(struct cfg_node **arr) {
  int i = 0;
  for (; arr[i]; i++)
    ;
  return i;
}
void write_ident() {}

void clear_visited_asm_flag(struct cfg_node *root) {
  for (int i = 0; root->connection_array[i]; i++) {
    if (root->connection_array[i]->is_visited_asm) {
      root->connection_array[i]->is_visited_asm = false;
      clear_visited_asm_flag(root->connection_array[i]);
    } else
      continue;
  }
  root->is_visited_asm = false;
}

#define FALSE_MISSING (void *)(1)

bool is_while_labels(labels_t *l) {
  if (!l)
    return false;
  if (l->next_instr)
    return true;
  return false;
}
bool is_labels_complete(labels_t *l) {
  if (!l)
    return false;
  if (l->true_branch && l->true_branch_end && l->false_branch &&
      l->true_branch_end && l->false_branch != FALSE_MISSING &&
      l->false_branch_end != FALSE_MISSING)
    return true;

  return false;
}
labels_t *get_last_labels(labels_t **label_arr) {
  labels_t *ptr = NULL;
  if (!label_arr)
    return ptr;

  for (int i = 0; label_arr[i] && (!is_labels_complete(label_arr[i]) ||
                                   is_while_labels(label_arr[i]));
       i++) {
    ptr = label_arr[i];
  }
  return ptr;
};
labels_t *get_last_labels_complete(labels_t **label_arr) {
  printf("get_last_labels_complete: label_arr = %p\n", label_arr);
  labels_t *ptr = NULL;

  for (int i = 0; label_arr[i]; i++) {
    if (is_labels_complete(label_arr[i]) || is_while_labels(label_arr[i]))
      ptr = label_arr[i];
  }
  for (int i = 0; label_arr[i]; i++) {
    printf("!!!!!!!!!!!!!!!!!!!\n");
    printf("tb: %p \n", label_arr[i]->true_branch);
    printf("fb: %p\n", label_arr[i]->false_branch);
    printf("tbe: %p\n", label_arr[i]->false_branch_end);
    printf("fbe: %p\n", label_arr[i]->false_branch_end);

    printf("!!!!!!!!!!!!!!!!!!!\n");
  }
  printf("get_last_labels_complete: result = %p\n", ptr);

  return ptr;
}

void insert_labels(labels_t **label_arr, labels_t *labels) {
  if (!label_arr)
    return;
  int i = 0;
  for (; label_arr[i]; i++)
    ;
  label_arr[i] = labels;
}

void delete_labels(labels_t **label_arr, labels_t *labels) {
  if (!label_arr)
    return;
  for (int i = 0; label_arr[i]; i++) {
    if (labels == label_arr[i]) {
      label_arr[i] = NULL;
      // free(labels);
    }
  }
}

void add_function(struct cfg_node *root) {}

struct instr_list *dfs_cfg_asm(struct cfg_node *node, instr_list_t *instr_list,
                               int *store_offset, struct cfg_node *prev_node,
                               labels_t **label_arr, bool test_flag,
                               char *fname) {
  labels_t *labels = test_flag ? get_last_labels_complete(label_arr)
                               : get_last_labels(label_arr);
  // if (labels) {
  //   printf("labels 1 = %s\n", labels->true_branch);
  //   printf("labels 2 = %s\n", labels->true_branch_end);
  //   printf("labels 3 = %s\n", labels->false_branch);
  //   printf("labels 4 = %s\n", labels->false_branch_end);
  // }
  printf("lptr = %p\n", labels);
  if (node == NULL)
    return NULL;
  int next_instr = 0;

  if (node->is_visited_asm)
    return NULL;

  node->is_visited_asm = true;

  switch (node->node->type) {
  case T_FUNC: {
    printf("cfg_asm: T_FUNC %p\n", node);
    // todo should to add

    struct ast_list *var_list =
        &node->node->as_func.body->as_body.var_list->as_list;

    struct ast_list *var_list2 =
        &node->node->as_func.func_sign->as_func_sign.arg_list->as_list;

    struct ast_node *name = node->node->as_func.func_sign->as_func_sign.ident;
    fname = name->as_ident.name;
    printf("%s\n", fname);
    // todo add .global and .type directive here
    // todo add func definition here
    if (!stable_arr)
      stable_arr = create_stable_f();

    fsymbol_t *s = stable_get_f(stable_arr, fname);
    printf("stable_ptr = %p\n", stable);
    if (!s || !s->stable) {
      printf("DEB1\n");
      stable = create_stable();
      stable_set_f(stable_arr,
                   (fsymbol_t){.name = fname, .node = node, .stable = stable});
    } else
      stable = s->stable;
    instr_list_t *list = malloc(sizeof(instr_list_t)); // todo should check NULL
    label_instr(list, fname);

    generate_prolog(list, var_list, var_list2);
    instr_list = list;
    printf("root node ptr = %p\n", instr_list);
    break;
  }
  case T_BRANCH: {
    printf("cfg_asm: T_BRANCH\n");
    struct cfg_node *test_cfg = cfg_create(node->body, NULL, NULL);
    //  create_cfg_node(node->body);
    instr_list_t tmp_list0 = {.head = NULL, .tail = NULL, .current = NULL};

    labels_t *labels_ptr = alloc_labels();
    insert_labels(label_arr, labels_ptr);

    if (connection_array_size(node->connection_array) < 2) {
      labels_ptr->false_branch = FALSE_MISSING;
      labels_ptr->false_branch_end = FALSE_MISSING;
    }

    instr_list_t tmp_list1 = {.head = NULL, .tail = NULL, .current = NULL};

    dfs_cfg_asm(node->connection_array[next_instr], &tmp_list1, NULL, node,
                label_arr, false, fname); // true
    next_instr++;

    instr_list_t tmp_list2 = {.head = NULL, .tail = NULL, .current = NULL};
    if (connection_array_size(node->connection_array) == 2) {

      dfs_cfg_asm(node->connection_array[next_instr], &tmp_list2, NULL, node,
                  label_arr, false, fname); // false
                                            // insert jump instruction
      branch_instr(&tmp_list1, AL, labels_ptr->false_branch_end);

      branch_instr(&tmp_list0, AL, labels_ptr->false_branch);

      next_instr++;
    } else if (labels) {
      printf("==========================\n");
      printf("labels 1 = %s\n", labels_ptr->true_branch);

      printf("labels 2 = %s\n", labels_ptr->true_branch_end);
      printf("labels 3 = %s\n", labels_ptr->false_branch);
      printf("labels 4 = %s\n", labels_ptr->false_branch_end);
      printf("==========================\n");
    }

    dfs_cfg_asm(test_cfg, instr_list, NULL, node, label_arr, true,
                fname); // test

    delete_labels(label_arr, labels_ptr);
    printf("==========================\n");
    printf("labels 1 = %s\n", labels_ptr->true_branch);
    printf("labels 2 = %s\n", labels_ptr->true_branch_end);
    printf("labels 3 = %s\n", labels_ptr->false_branch);
    printf("labels 4 = %s\n", labels_ptr->false_branch_end);
    printf("==========================\n");
    if (labels) {
      if (!labels->true_branch_end) {
        labels->true_branch_end = labels_ptr->false_branch_end;
      }
      printf("==========================\n");
      printf("labels 1 = %s\n", labels->true_branch);
      printf("labels 2 = %s\n", labels->true_branch_end);
      printf("labels 3 = %s\n", labels->false_branch);
      printf("labels 4 = %s\n", labels->false_branch_end);
      printf("==========================\n");
    }

    MERGE_LIST(instr_list, &tmp_list0);
    MERGE_LIST(instr_list, &tmp_list1);
    MERGE_LIST(instr_list, &tmp_list2);

    break;
  }
  case T_REPEAT:
  case T_WHILE: {
    printf("cfg_asm: T_WHILE\n");
    struct cfg_node *ret_val = NULL;
    struct cfg_node *test_cfg = cfg_create(node->body, NULL, NULL);
    if (!test_flag && labels &&
        (!labels->true_branch ||
         (labels->true_branch && labels->true_branch_end &&
          !labels->false_branch)))
      set_label(labels, instr_list);
    // create_cfg_node(node->body);
    // ret_val = cfg_create(node->body, NULL, NULL);

    labels_t *labels_ptr = alloc_labels();
    insert_labels(label_arr, labels_ptr);

    labels_ptr->false_branch = FALSE_MISSING;
    labels_ptr->false_branch_end = FALSE_MISSING;

    instr_list_t tmp_list = {.head = NULL, .tail = NULL, .current = NULL};

    char *label_to_cond = generate_label();
    label_instr(instr_list, label_to_cond);
    labels_ptr->next_instr = label_to_cond;

    dfs_cfg_asm(node->connection_array[next_instr], &tmp_list, NULL, node,
                label_arr, false, fname);
    next_instr++;

    labels_ptr->false_branch = labels_ptr->true_branch_end;

    // branch_instr(&tmp_list, AL, labels_ptr->true_branch);
    //  labels_ptr->false_branch_end = FALSE_MISSING;

    dfs_cfg_asm(test_cfg, instr_list, NULL, node, label_arr, true, fname);
    branch_instr(instr_list, AL, labels_ptr->true_branch_end);

    delete_labels(label_arr, labels_ptr);
    if (labels) {
      if (!labels->true_branch_end) {
        labels->true_branch_end = labels_ptr->true_branch_end;
      }
    }
    MERGE_LIST(instr_list, &tmp_list);
    // branch_instr(instr_list, AL, labels_ptr->true_branch);

    break;
  }
  case T_CALL_EXPR: {
    printf("cfg_asm: T_CALL_EXPR\n");
    struct ast_node *args = node->node->as_call_expr.expr_list;
    char *name =
        node->node->as_call_expr.expr->as_expr.some_node->as_ident.name;
    printf("name = %s\n", name);
    // struct cfg_node *croot = stable_get_f(stable_arr, name)->node;
    // printf("croot = %p\n", croot);
    // labels_t **larr = calloc(100, sizeof(labels_t *));
    // instr_list_t il = {.head = NULL, .tail = NULL, .current = NULL};
    // clear_visited_asm_flag(croot);
    // dfs_cfg_asm(croot, &il, NULL, NULL, larr, false, NULL);
    // printf("EXIT\n");
    // stable = stable_get_f(stable_arr, fname)->stable;
    // MERGE_LIST(instr_list, &il);
    instr_node_t *instr = create_instr_node(JMP);
    instr->instruction.jmi =
        (jmi_t){.instr_type = BLI, .cond = AL, .label = name};
    printf("AAA\n");
    ADD_TO_INSTR_LIST(instr_list, instr);
    break;
  }
  case T_BIN_EXPR: {
    printf("cfg_asm: T_BIN_EXPR\n");
    if (!test_flag && labels &&
        (!labels->true_branch ||
         (labels->true_branch && labels->true_branch_end &&
          !labels->false_branch)))
      set_label(labels, instr_list);

    switch (node->node->as_binexpr.op) {
    case OT_BIN_PLUS: {
      printf("cfg_asm: PLUS\n");

      // const symbol_t *op1 = stable_get(stable,
      // node->node->as_binexpr.arg1); const symbol_t *op2 =
      // stable_get(stable, node->node->as_binexpr.arg2);

      // read_ident(op1, instr_list, R0);
      // read_ident(op2, instr_list, R1);

      dfs_cfg_asm(node->connection_array[next_instr], instr_list, NULL, node,
                  label_arr, false, fname);
      next_instr++;
      dfs_cfg_asm(node->connection_array[next_instr], instr_list, NULL, node,
                  label_arr, false, fname);
      next_instr++;
      pop_word(instr_list, R0);
      pop_word(instr_list, R1);

      instr_node_t *instr = create_instr_node(DP);

      instr->instruction.dpi = (dpi_t){.instr_type = ADD,
                                       .s = NONE,
                                       .cond = AL,
                                       .rd = R0,
                                       .op1 = REG,
                                       .op2 = REG,
                                       .op1_reg = R0,
                                       .op2_reg = R1};
      ADD_TO_INSTR_LIST(instr_list, instr);
      push_word(instr_list, R0);
      break;
    }
    case OT_MINUS: {
      printf("cfg_asm: MINUS\n");

      dfs_cfg_asm(node->connection_array[next_instr], instr_list, NULL, node,
                  label_arr, false, fname);
      next_instr++;
      dfs_cfg_asm(node->connection_array[next_instr], instr_list, NULL, node,
                  label_arr, false, fname);
      next_instr++;

      pop_word(instr_list, R1);
      pop_word(instr_list, R0);

      instr_node_t *instr = create_instr_node(DP);

      instr->instruction.dpi = (dpi_t){.instr_type = SUB,
                                       .s = NONE,
                                       .cond = AL,
                                       .rd = R0,
                                       .op1 = REG,
                                       .op2 = REG,
                                       .op1_reg = R0,
                                       .op2_reg = R1};
      ADD_TO_INSTR_LIST(instr_list, instr);
      push_word(instr_list, R0);
      break;
    }
    case OT_ASSIGN: {
      printf("cfg_asm: ASSIGN\n");

      int offs = 0;
      dfs_cfg_asm(node->connection_array[next_instr], instr_list, &offs, node,
                  label_arr, false, fname);
      next_instr++;

      dfs_cfg_asm(node->connection_array[next_instr], instr_list, NULL, node,
                  label_arr, false, fname);
      next_instr++;

      pop_word(instr_list, R1);
      pop_word(instr_list, R0);

      instr_node_t *instr1 = create_instr_node(DP);

      instr1->instruction.dpi = (dpi_t){.instr_type = ADD,
                                        .s = NONE,
                                        .cond = AL,
                                        .rd = R0,
                                        .op1 = REG,
                                        .op2 = IMV,
                                        .op1_reg = R1,
                                        .op2_imm_val = 0};
      ADD_TO_INSTR_LIST(instr_list, instr1);
      instr_node_t *instr2 = create_instr_node(STI);
      instr2->instruction.sti = (sti_t){
          .instr_type = STR,
          .cond = AL,
          .rt = R0,
          .rn = SP,
          .sot = WL,
          .offset = offs,
      };

      ADD_TO_INSTR_LIST(instr_list, instr2);
      break;
    }
    case OT_BIN_GREATER: {
      printf("cfg_asm: OT_BIN_GREATER\n");
      dfs_cfg_asm(node->connection_array[next_instr], instr_list, NULL, node,
                  label_arr, false, fname);
      next_instr++;

      dfs_cfg_asm(node->connection_array[next_instr], instr_list, NULL, node,
                  label_arr, false, fname);
      next_instr++;

      pop_word(instr_list, R1);
      pop_word(instr_list, R0);
      cmp(instr_list, R0, R1);

      printf("bge ptr = %p\n", labels);
      if (labels->true_branch) {
        printf("AAAAAAAAAA\n");
        branch_instr(instr_list, GE, labels->true_branch);
        //  if (labels->false_branch && labels->false_branch != FALSE_MISSING)
        //  {
        //    branch_instr(instr_list, AL, labels->false_branch);
        //  }
      }

      break;
    }

    case OT_BIN_LESS: {
      printf("cfg_asm: OT_BIN_LESS\n");
      dfs_cfg_asm(node->connection_array[next_instr], instr_list, NULL, node,
                  label_arr, false, fname);
      next_instr++;

      dfs_cfg_asm(node->connection_array[next_instr], instr_list, NULL, node,
                  label_arr, false, fname);
      next_instr++;
      pop_word(instr_list, R1);
      pop_word(instr_list, R0);
      cmp(instr_list, R0, R1);
      if (labels->true_branch) {
        branch_instr(instr_list, LE, labels->true_branch);
        // if (labels->false_branch) {
        //   branch_instr(instr_list, AL, labels->false_branch);
        // }
      }

      break;
    }
    case OT_BIN_EQUALS: {
      printf("cfg_asm: OT_BIN_EQUALS\n");
      dfs_cfg_asm(node->connection_array[next_instr], instr_list, NULL, node,
                  label_arr, false, fname);
      next_instr++;

      dfs_cfg_asm(node->connection_array[next_instr], instr_list, NULL, node,
                  label_arr, false, fname);
      next_instr++;
      pop_word(instr_list, R1);
      pop_word(instr_list, R0);
      cmp(instr_list, R0, R1);
      if (labels->true_branch) {
        branch_instr(instr_list, EQ, labels->true_branch);
        // if (labels->false_branch) {
        //   branch_instr(instr_list, AL, labels->false_branch);
        // }
      }

      break;
    }
    case OT_BIN_MUL: {
      printf("cfg_asm: OT_BIN_MUL\n");
      break;
    }
    case OT_BIN_DIV: {
      printf("cfg_asm: OT_BIN_DIV\n");
      break;
    }

    default:
      break;
    }
    if (!test_flag && labels && labels->next_instr) {
      printf("set Branch\n");

      branch_instr(instr_list, AL, labels->next_instr);
      set_label(labels, instr_list);
    }
    // if (!test_flag)
    //   set_label(labels, instr_list);
    // else

    //   for (; node->connection_array[next_instr]; next_instr++) { // todo fix
    //   this
    //     printf("B1\n");
    //     struct cfg_node *next = node->connection_array[next_instr];
    //     if (get_parents_num(next) == 2) {
    //       printf("B2\n");
    //       set_label(labels, instr_list);
    //       printf("labels = %p\n", labels);
    //       if (!test_flag && labels && !labels->false_branch) {
    //         printf("B3\n");
    //         return instr_list; // удаляем дублирование кода при true branch
    //       } else if (!test_flag && labels &&
    //                  labels->false_branch == FALSE_MISSING) {
    //         printf("B4\n");
    //         labels->false_branch = generate_label();
    //         labels->false_branch_end = generate_label();
    //         label_instr(instr_list, labels->false_branch);
    //         label_instr(instr_list, labels->false_branch_end);
    //       }

    //       dfs_cfg_asm(next, instr_list, NULL, node, label_arr, false);
    //     } else {

    //       dfs_cfg_asm(next, instr_list, NULL, node, label_arr, false);
    //     }
    //   }

    for (; node->connection_array[next_instr]; next_instr++) { // todo fix this
      printf("B1\n");
      struct cfg_node *next = node->connection_array[next_instr];
      int pn = get_parents_num(next);
      bool flag = false;
      int i = 0;
      for (; next->parents[i]; i++) {
        if (next->parents[i] == node) {
          printf("MATCH\n");
          break;
        }
      }
      if (pn - i == 1)
        flag = true;
      if (pn >= 2) {

        printf("B2\n");
        set_label(labels, instr_list);
        if (!test_flag && !flag) {
          printf("B3\n");
          return instr_list; // удаляем дублирование кода
        } else if (!test_flag && labels &&
                   labels->false_branch == FALSE_MISSING) {
          printf("B4\n");
          // labels->false_branch = generate_label();
          // labels->false_branch_end = generate_label();
          // label_instr(instr_list, labels->false_branch);
          // label_instr(instr_list, labels->false_branch_end);
        } else if (!test_flag && flag) {
          dfs_cfg_asm(next, instr_list, NULL, node, label_arr, false, fname);
        }

        // dfs_cfg_asm(next, instr_list, NULL, node, label_arr, false);
      } else {
        dfs_cfg_asm(next, instr_list, NULL, node, label_arr, false, fname);
      }
    }

    break;
  }
  case T_IDENT: {
    printf("cfg_asm: T_IDENT name = %s\n", node->node->as_ident.name);
    const symbol_t *var = stable_get(stable, node->node);
    read_ident(var, instr_list, R0);
    if (store_offset)
      *store_offset = var->offset;
    push_word(instr_list, R0);
    break;
  }
  case T_LITERAL: {
    switch (node->node->as_literal.type->as_typeref_lt.type) {
    case LT_DEC: {

      uint32_t val = atoi(node->node->as_literal.value);
      printf("literal LT_DEC = %d\n", val);
      mov_imm_instr(instr_list, R0, (uint16_t)(val & 0xFFFFUL));
      movt_instr(instr_list, R0, (uint16_t)((val >> 16) & 0xFFFFUL));

      push_word(instr_list, R0);
      break;
    }
    default:
      printf("unknown literal type\n");
    }
    break;
  }
  default:
    break;
  }
  for (; node->connection_array[next_instr]; next_instr++) {
    dfs_cfg_asm(node->connection_array[next_instr], instr_list, NULL, node,
                label_arr, false, fname);
  }
  if (node->node->type == T_FUNC) {
    generate_epiloge(instr_list);
  }
  return instr_list;
};
