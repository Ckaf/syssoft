#include "asm.h"
#include <stdio.h>

instr_node_t *create_instr_node(enum mnemonic_type m) {
  instr_node_t *i = (instr_node_t *)(malloc(sizeof(struct instr_node)));
  i->instruction.m_type = m;
  return i;
}

void print_instr(const instr_node_t *instr) {

  switch (instr->instruction.m_type) {
  case DP: {
    // printf("<DP> ");
    dpi_t i = instr->instruction.dpi;
    char op2_buff[10];

    if (i.op2 == REG)
      strcpy(op2_buff, register_type_names[i.op2_reg]);
    else {
      int x = i.op2_imm_val;
      int length = snprintf(NULL, 0, "#%d", x);
      snprintf(op2_buff, length + 1, "#%d", x);
    }
    if (i.instr_type == MOVT)
      break;
    if (i.instr_type == MOV) {
      printf("%s%s%s %s, %s\n", data_proc_type_names[i.instr_type],
             s_type_names[i.s], cond_type_names[i.cond],
             register_type_names[i.rd], op2_buff);
    } else if (i.instr_type == CMP) {
      printf("%s%s%s %s, %s\n", data_proc_type_names[i.instr_type],
             s_type_names[i.s], cond_type_names[i.cond],
             register_type_names[i.op1_reg], op2_buff);

    } else
      printf("%s%s%s %s, %s, %s\n", data_proc_type_names[i.instr_type],
             s_type_names[i.s], cond_type_names[i.cond],
             register_type_names[i.rd], register_type_names[i.op1_reg],
             op2_buff);

    break;
  };
  case ST: {
    // printf("<ST> ");
    printf("\n");

    break;
  }
  case ML: {
    // printf("<ML> ");
    printf("\n");

    break;
  }
  case LDI: {
    // printf("<LDI> ");
    ldi_t i = instr->instruction.ldi;
    printf("%s%s %s, [%s, #%d]", load_type_names[i.instr_type],
           ls_op_type_names[i.lot], register_type_names[i.rt],
           register_type_names[i.rn], i.offset);

    printf("\n");

    break;
  }
  case STI: {
    // printf("<STI> ");
    sti_t i = instr->instruction.sti;
    printf("%s%s%s %s, [%s, #%d]", store_type_names[i.instr_type],
           ls_op_type_names[i.sot], cond_type_names[i.cond],
           register_type_names[i.rt], register_type_names[i.rn], i.offset);
    printf("\n");

    break;
  }
  case JMP: {
    // printf("<JMP> ");
    jmi_t i = instr->instruction.jmi;
    printf("%s%s %s", jump_type_names[i.instr_type], cond_type_names[i.cond],
           i.label);
    printf("\n");

    break;
  }
  case SKI: {

    // printf("<SKI> ");
    ski_t i = instr->instruction.ski;
    printf("%s {", stack_type_names[i.instr_type]);
    for (int j = 0; j < i.reglist_size; j++) {
      if (j)
        printf(",%s", register_type_names[i.reglist[j]]);
      else
        printf("%s", register_type_names[i.reglist[j]]);
    }
    printf("}\n");

    break;
  }
  case LABEL: {
    // printf("<LABEL> ");
    printf(".%s:\n", instr->instruction.label);
  }
  }
}

void print_instr_list(const instr_list_t *instr_list) {

  instr_node_t *instr_iter = instr_list->head;
  printf("===================\n");
  while (instr_iter) {
    print_instr(instr_iter);
    instr_iter = instr_iter->next;
  };
  printf("===================\n");
}

labels_t *alloc_labels() {
  labels_t *labels = (labels_t *)malloc(sizeof(labels_t));
  *labels = (labels_t){.true_branch = NULL,
                       .true_branch_end = NULL,
                       .false_branch = NULL,
                       .false_branch_end = NULL};
  return labels;
};
static int labels_cnt = 0;
char *generate_label() {
  char *label = malloc(sizeof(char) * 10);
  sprintf(label, "label%d", labels_cnt++);
  return label;
};
