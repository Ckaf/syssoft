#include "asm.h"
#include "ast.h"
void dfs_bypass(struct ast_node *node, process_cb preproccess_cb,
                process_cb postprocess_cb);

struct cfg_node *cfg_create(struct ast_node *node, struct cfg_node *prev_cfg,
                            struct cfg_node *next_node);

struct instr_list *dfs_cfg_asm(struct cfg_node *node, instr_list_t *instr_list,
                               int *store_offset, struct cfg_node *prev_node,
                               labels_t **label_arr, bool test_flag,
                               char *fname);

void add_function(struct cfg_node *root);
