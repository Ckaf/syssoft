#include "symbol_table.h"
#include "ast.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

static int symbol_compare(const void *a, const void *b, void *udata) {
  const symbol_t *sa = a;
  const symbol_t *sb = b;
  return strcmp(sa->node->as_ident.name, sb->node->as_ident.name);
}

static int fsymbol_compare(const void *a, const void *b, void *udata) {
  const fsymbol_t *sa = a;
  const fsymbol_t *sb = b;
  return strcmp(sa->name, sb->name);
}

static uint64_t symbol_hash(const void *item, uint64_t seed0, uint64_t seed1) {
  const symbol_t *s = item;
  const char *name;
  if (s->node->type == T_IDENT) {
    name = s->node->as_ident.name;
  }
  return hashmap_sip(name, strlen(name), seed0, seed1);
}

static uint64_t fsymbol_hash(const void *item, uint64_t seed0, uint64_t seed1) {
  const fsymbol_t *s = item;
  const char *name = s->name;
  return hashmap_sip(name, strlen(name), seed0, seed1);
}

static uint64_t addr_cnt;

stable_t *create_stable() {
  addr_cnt = 0;
  struct hashmap *map = hashmap_new(sizeof(symbol_t), 0, 0, 0, symbol_hash,
                                    symbol_compare, NULL, NULL);
  return map;
};
stable_t *create_stable_f() {
  addr_cnt = 0;
  struct hashmap *map = hashmap_new(sizeof(fsymbol_t), 0, 0, 0, fsymbol_hash,
                                    fsymbol_compare, NULL, NULL);
  return map;
};
static uint64_t generate_address() {
  addr_cnt += 64; // пока пусть так todo
  return addr_cnt;
}

const void *stable_set(stable_t *st, symbol_t n) {
  symbol_t *symb = malloc(sizeof(n));
  symb = &n;
  return hashmap_set(st, symb);
};

const symbol_t *stable_get(stable_t *st, struct ast_node *n) {
  const symbol_t *res = hashmap_get(st, &(symbol_t){.node = n});
  return res;
}

const void *stable_set_f(stable_t *st, fsymbol_t n) {
  fsymbol_t *symb = malloc(sizeof(n));
  symb = &n;
  return hashmap_set(st, symb);
};

const fsymbol_t *stable_get_f(stable_t *st, char *n) {
  const fsymbol_t *res = hashmap_get(st, &(fsymbol_t){.name = n});
  return res;
}

void stable_free(stable_t *st) { hashmap_free(st); }
