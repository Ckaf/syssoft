#include "hashmap.h"
#include <stdint.h>

typedef struct hashmap stable_t;

typedef struct symbol {
  struct ast_node *node;
  uint64_t offset;
  uint16_t size;
} symbol_t;

typedef struct func_symbol {
  char *name;
  struct cfg_node *node;
  stable_t *stable;
} fsymbol_t;

stable_t *create_stable();

stable_t *create_stable_f();

const void *stable_set(stable_t *st, symbol_t n);

const symbol_t *stable_get(stable_t *st, struct ast_node *n);

const void *stable_set_f(stable_t *st, fsymbol_t n);

const fsymbol_t *stable_get_f(stable_t *st, char *n);

void stable_free(stable_t *st);
