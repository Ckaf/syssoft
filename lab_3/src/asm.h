// #include "ast.h"
//  #include <cstdlib>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum components_type { MNEMONIC, S, CONDITION, RD, OP1, OP2 };

enum s_type { NONE, UPDATE_FLAGS };

static const char *s_type_names[] = {[NONE] = "", [UPDATE_FLAGS] = "s"};

enum op_type {
  IMV, // immediate value
  RSV, // register shifted by value only for op2
  RSR, // register shifted by register only for op2
  REG  // just register
};

enum condition_type {
  EQ, //  Equal
  NE, // 	Not Equal
  GT, // Signed Greater Than
  LT, // 	Signed Less Than
  GE, // Signed Greater Than or Equal
  LE, // Signed Less Than or Equal
  CS, // Unsigned Higher or Same (or Carry Set
  // todo maybe HS
  CC, //	Unsigned Lower (or Carry Clear)
  // todo maybe LO
  MI, //  Negative (or Minus)
  PL, // 	Positive (or Plus)
  AL, // 	Always executed
  NV, // 	Never executed
  VS, // Signed Overflow
  VC, // 	No signed Overflow
  HI, // 	Unsigned Higher
  LS  // 	Unsigned Lower or same
};

static const char *cond_type_names[] = {
    [EQ] = "eq", [NE] = "ne", [GT] = "gt", [LT] = "lt",
    [GE] = "ge", [LE] = "le", [CS] = "cs", [CC] = "cc",
    [MI] = "mi", [PL] = "pl", [AL] = "",   [NV] = "nv",
    [VS] = "vs", [VC] = "vc", [HI] = "hi", [LS] = "ls"};

#define FP R11
#define IP R12
#define SP R13
#define LR R14
#define PC R15

enum register_type {
  R0,   // General purpose
  R1,   // General purpose
  R2,   // General purpose
  R3,   // General purpose
  R4,   // General purpose
  R5,   // General purpose
  R6,   // General purpose
  R7,   // Holds Syscall Number
  R8,   // General purpose
  R9,   // General purpose
  R10,  // General purpose
  R11,  // Frame Pointer
  R12,  // Intra Procedural Call
  R13,  // Stack Pointer
  R14,  // Link Register
  R15,  // Program Counter
  CPSR, // Current Program Status Register
  REG_NONE
};

static const char *register_type_names[] = {
    [R0] = "r0",     [R1] = "r1",    [R2] = "r2",   [R3] = "r3",
    [R4] = "r4",     [R5] = "r5",    [R6] = "r6",   [R7] = "r7",
    [R8] = "r8",     [R9] = "r9",    [R10] = "r10", [R11] = "r11",
    [R12] = "r12",   [R13] = "r13",  [R14] = "r14", [R15] = "r15",
    [CPSR] = "cpsr", [REG_NONE] = ""};

enum data_processing_type {

  // Arithmetic operations
  ADC,  // Add with carry
  ADD,  // Add
  MOV,  // Move
  MOVT, // Move top
  MVN,  // Move NOT
  RSB,  // Reverse Subtract
  RSC,  // Reverse Subtract with Carry
  SBC,  // Subtract with carry
  SUB,  // Subtract

  // Logical operations
  AND, // AND
  BIC, // Bit Clear
  EOR, // Exclusive OR
  ORR, // OR

  // Flag setting instructions
  CMP, // Compare
  CMN, // Compare Negative
  TEQ, // Test EQuivalence
  TST  // Test
};

static const char *data_proc_type_names[] = {
    [ADC] = "adc", [ADD] = "add", [MOV] = "mov", [MOVT] = "movt", [MVN] = "mvn",
    [RSB] = "rsb", [RSC] = "rsc", [SBC] = "sbc", [SUB] = "sub",   [AND] = "and",
    [BIC] = "bic", [EOR] = "eor", [ORR] = "orr", [CMP] = "cmp",   [CMN] = "cmn",
    [TEQ] = "teq", [TST] = "tst"};

enum shift_type {
  ASR, // Arithmetic Shift Right
  LSL, // Shift Logical Shift
  LSR, // Shift Logical Right
  ROR, // Rotate right Register
  RRX  // Rotate Right with extend
};

static const char *shift_type_names[] = {
    [ASR] = "asr", [LSL] = "lsl", [LSR] = "lsr", [ROR] = "ror", [RRX] = "rrx"};

enum mult_type {
  MLA,   // Multiply accumulate (MAC)
  MLS,   // Multiply and Subtract
  MUL,   // Multiply
  SMLAL, // Signed 32-bit multiply with a 64-bit accumulate
  SMULL, // Signed 64-bit multiply
  UMLAL, // Unsigned 64-bit MAC
  UMULL  // Unsigned 64-bit multiply
};

static const char *mult_type_names[] = {
    [MLA] = "mla",     [MLS] = "mls",     [MUL] = "mul",    [SMLAL] = "smlal",
    [SMULL] = "smull", [UMLAL] = "umlal", [UMULL] = "umull"};

enum load_type { LDR, LDM };

static const char *load_type_names[] = {[LDR] = "ldr", [LDM] = "ldm"};

enum store_type { STR };

static const char *store_type_names[] = {[STR] = "str"};

enum stack_type { PUSH, POP };

static const char *stack_type_names[] = {[PUSH] = "push", [POP] = "pop"};

enum jump_type { B, BX, BLI };

static const char *jump_type_names[] = {[B] = "b", [BX] = "bx", [BLI] = "bl"};

enum mnemonic_type {
  DP,  // data processing
  ST,  // shift
  ML,  // mul
  LDI, // load
  STI, // store
  JMP, // jump
  SKI, // stack
  LABEL
};

struct shift_by {
  enum shift_type instr_type;
  enum register_type rx;
  union {
    uint32_t imm_val;
    enum register_type reg;
  };
};

typedef struct dp_instr {
  enum data_processing_type instr_type;
  enum s_type s; // for default should be NONE
  enum register_type rd;
  enum condition_type cond; // for default should be AL

  enum op_type op1;
  union {
    enum register_type op1_reg;
    uint32_t op1_imm_val;
  };

  enum op_type op2;
  union {
    enum register_type op2_reg;
    uint32_t op2_imm_val;
  };
} dpi_t;

typedef struct shift_by shi_t; // shift instruction

typedef struct mult_instr { // todo add support SMLAL, SMULL, UMLAL, UMULL

  enum mult_type instr_type;
  enum s_type s;
  enum condition_type cond;
  enum register_type rd; // destination register
  enum register_type rn; // register holding the first multiplicand
  enum register_type rm; // register holding the second multiplicand.
  enum register_type ra; // register holding the accumulate value.

} mti_t;

enum ls_op_type {
  BL,  // unsigned Byte. (Zero extend to 32 bits on loads.)
  SBL, // signed Byte. (Sign extend to 32 bits.)
  HL,  // unsigned Halfword. (Zero extend to 32 bits on loads.)
  SHL, // signed Halfword. (Sign extend to 32 bits.)
  WL   // by default for word load
};

static const char *ls_op_type_names[] = {
    [BL] = "b", [SBL] = "sb", [HL] = "h", [SHL] = "sh", [WL] = ""};

typedef struct jmp_instr {
  enum jump_type instr_type;
  enum condition_type cond;
  char *label;
} jmi_t;

typedef struct load_instr {
  enum load_type instr_type;
  enum condition_type cond;
  enum ls_op_type lot;
  enum register_type rt;
  enum register_type rn;
  enum register_type rm;
  uint32_t offset;
  shi_t shi; // todo add support

} ldi_t;

typedef struct stack_instr {
  enum stack_type instr_type;
  enum register_type *reglist; // pointer to array of registers
  int reglist_size;
} ski_t;

typedef struct store_instr {
  enum store_type instr_type;
  enum ls_op_type sot;
  enum condition_type cond;
  enum register_type rt;
  enum register_type rn;
  enum register_type rm;
  uint32_t offset;
} sti_t;

typedef struct instruction {
  enum mnemonic_type m_type;
  union {
    dpi_t dpi;
    shi_t shi;
    mti_t mti;
    ldi_t ldi;
    ski_t ski;
    sti_t sti;
    jmi_t jmi;
    char *label;
  };

} instr_t;

enum instr_node_type { PROLOG, EPILOG, BODY };

typedef struct instr_node {
  enum instr_node_type type;
  instr_t instruction;
  struct instr_node *next;
} instr_node_t;

instr_node_t *create_instr_node(enum mnemonic_type m);

typedef struct instr_list {
  instr_node_t *head;
  instr_node_t *current;
  instr_node_t *tail;
} instr_list_t;

#define DEFAULT_FRAME_SIZE 36

// todo fix that
#define TR_IDENT_SIZE 4
#define TR_BOOL_SIZE 1
#define TR_BYTE_SIZE 1
#define TR_INT_SIZE 4
#define TR_UINT_SIZE 4
#define TR_LONG_SIZE 4
#define TR_ULONG_SIZE 4
#define TR_CHAR_SIZE 1
// todo fix str_size and array
#define TR_STR_SIZE 20
#define TR_ARR_SIZE 20
#define TR_NONE_SIZE 4

void print_instr(const instr_node_t *instr);

void print_instr_list(const instr_list_t *instr_list);

typedef struct labels {
  char *true_branch;
  char *true_branch_end;
  char *false_branch;
  char *false_branch_end;
  char *next_instr;
} labels_t;

labels_t *alloc_labels();
char *generate_label();
