# Lab 3
For this laboratory work I decided to take a processor of the ARM Cortex-A series.
## Memory model description
> The ARM architecture provides sixteen 32-bit general purpose registers (R0-R15) for software
> use. Fifteen of them (R0-R14) can be used for general purpose data storage, while R15 is the
> program counter whose value is altered as the core executes instructions. An explicit write to
> R15 by software will alter program flow. Software can also access the CPSR, and a saved copy
> of the CPSR from the previously executed mode, called the Saved Program Status Register
> (SPSR).
> R13 (in all modes) is the OS stack pointer, but it can be used as a general purpose register when
> not required for stack operations.
> R14 (the Link Register) holds the return address from a subroutine entered when you use the
> branch with link (BL) instruction. It too can be used as a general purpose register when it is not
> supporting returns from subroutines. R14_svc, R14_irq, R14_fiq, R14_abt and R14_und are
> used similarly to hold the return values of R15 when interrupts and exceptions arise, or when
> Branch and Link instructions are executed within interrupt or exception routines.

And a few more words about the Program Status Register...

> Application programmers must use the APSR to access the parts of the CPSR that can be
> changed in unprivileged mode. The APSR must be used only to access the N, Z, C, V, Q, and
> GE[3:0] bits. These bits are not normally accessed directly, but instead set by condition code
> setting instructions and tested by instructions that are executed conditionally.
> For example, the `CMP R0, R1` instruction compares the values of R0 and R1 and sets the zero flag
> (Z) if R0 and R1 are equal. 

The flags field holds four status bits: negative (N), zero (Z), carry \(C\), and overflow (V).

## ASM instructions

### Data processing operations
| opcode | operands | Description | Function |
| ------ | -------- | ----------- | -------- |
| Arithmetic operations |
| ADC | Rd, Rn, Op2 | Add with carry              | Rd = Rn + Op2 + C |
| ADD | Rd, Rn, Op2 | Add                         | Rd = Rn + Op2  |
| MOV | Rd, Op2     | Move                        | Rd = Op2  |
| MVN | Rd, Op2     | Move NOT                    | Rd = ~Op2  |
| RSB | Rd, Rn, Op2 | Reverse Subtract            | Rd = Op2 – Rn |
| RSC | Rd, Rn, Op2 | Reverse Subtract with Carry | Rd = Op2 – Rn - !C |
| SBC | Rd, Rn, Op2 | Subtract with carry         | Rd = Rn – Op2 -!C  |
| SUB | Rd, Rn, Op2 | Subtract                    | Rd = Rn – Op2 |
| Logical operations |
| AND | Rd, Rn, Op2 | AND          | Rd = Rn & Op2  |
| BIC | Rd, Rn, Op2 | Bit Clear    | Rd = Rn & ~ Op2 |
| EOR | Rd, Rn, Op2 | Exclusive OR | Rd = Rn ^ Op2 |
| ORR | Rd, Rn, Op2 | OR           | Rd = Rn \| Op2  ->  (OR NOT) ->   Rd = Rn \| ~Op2 |
| Flag setting instructions |
| CMP | Rn, Op2 | Compare          | Rn – Op2 |
| CMN | Rn, Op2 | Compare Negative | Rn + Op2 |
| TEQ | Rn, Op2 | Test EQuivalence | Rn ^ Op2 |
| TST | Rn, Op2 | Test             | Rn & Op2 |

#### Data types


#### Endianness
>The ARM architecture was little-endian before version 3, since then it is bi-endian, 
>which means that it features a setting which allows for switchable endianness. 
>On ARMv6 for example, instructions are fixed little-endian and data accesses can be either 
>little-endian or big-endian as controlled by bit 9, the E bit, of the Program Status Register (CPSR).



```
identifier: "[a-zA-Z_][a-zA-Z_0-9]*"; // идентификатор
str: "\"[^\"\\]*(?:\\.[^\"\\]*)*\""; // строка, окруженная двойными кавычками
char: "'[^']'";
// одиночный символ в одинарных кавычках
hex: "0[xX][0-9A-Fa-f]+";
// шестнадцатеричный литерал
bits: "0[bB][01]+";
// битовый литерал
dec: "[0-9]+";
// десятичный литерал
bool: 'true'|'false';
// булевский литерал
list<item>: (item (',' item)*)?; // список элементов, разделённых запятыми

source: sourceItem*;
typeRef: {
|builtin: 'bool'|'byte'|'int'|'uint'|'long'|'ulong'|'char'|'string';
|custom: identifier;
|array: 'array' '[' (',')* ']' 'of' typeRef;
};
funcSignature: identifier '(' list<argDef> ')' (':' typeRef)? {
argDef: identifier (':' typeRef)?;
};
sourceItem: {
|funcDef: 'method' funcSignature (body|';') {
body: ('var' (list<identifier> (':' typeRef)? ';')*)? statement.block;
};
statement: {
|if: 'if' expr 'then' statement ('else' statement)?;
|block: 'begin' statement* 'end' ';';
|while: 'while' expr 'do' statement;
|do: 'repeat' statement ('while'|'until') expr ';';
|break: 'break' ';';
|expression: expr ';';
};
expr: { // присваивание через ':='
|binary: expr binOp expr; // где binOp - символ бинарного оператора
|unary: unOp expr; // где unOp - символ унарного оператора
|braces: '(' expr ')';
|call: expr '(' list<expr> ')';
|indexer: expr '[' list<expr> ']';
|place: identifier;
|literal: bool|str|char|hex|bits|dec;
};
```
